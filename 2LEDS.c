// Flip Flop with Arduino

// LED names => Pin numbers
int led08 = 8;
int led12 = 12;

void setup() {                
  pinMode(led08, OUTPUT); 
  pinMode(led12, OUTPUT);      
}

void loop() {
  digitalWrite(led08, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(led12, LOW);   // turn the LED on (HIGH is the voltage level)

  delay(1000);               // wait for a second

  digitalWrite(led08, LOW);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(led12, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
}
